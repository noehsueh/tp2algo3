#include <iostream>
#include <iomanip>
#include "Graph.h"

// main pra usarse desde clion

int main() {
//    for (int i = 0; i < 6; ++i) {
//        string file;
////        file = "../instancias/instancia" + to_string(i) + ".txt";
//        file = "../geodesicas/instancia_" + to_string(i) + ".txt";
//        Graph G(file);
////        cout << file << endl;
////        bool res = G.esGeodesico();
////        cout << res << endl;
//    }
    string file;
//    file = "../instancias/instancia_" + to_string(0) + ".txt";
    file = "../instancias/instancia_4.txt";
    Graph G(file);
    cout << file << endl;
    G.esGeodesico();
    return 0;
}

//// main para usarse desde la consola
//int main(int argc, char** argv) {
//    Graph G(argv[1]);
//    G.esGeodesico();
//    return 0;
//}